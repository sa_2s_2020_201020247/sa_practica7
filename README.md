# PRACTICA 7

_APIS elaborado en .Net Core en su versión 2.1, en dicha carpeta se tiene el contenido del artefacto del Servicio del ESBApi visto en las anteriores entregas, esto depositado automaticamente desde un pipeline de Jenkins_

_La razón de esta practica automatizar todo el flujo de pruebas, analisis de código, creación de artefacto y su publicación_

## Información General
- Practica 7 Laboratorio Software Avanzado
- Creado por Haroldo Arias
- Carnet 201020247
- Septiembre 2020
- Ver el video para ver la demostración del pipeline local de Jenkins (El archivo del log se llama Jenkins.log)

## Video de Demostración
_Video de la demostración de la funcionalidad de estos servicios, como una rápida explicacion de su estructura_
* [Video](https://drive.google.com/file/d/1SgHNvngpg0rJ60EvhuD5U-JTyAWm_wGb/view?usp=sharing) - Acá puedes verlo

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋
_Debes de tener instalado el SDK del .NetCore en su versión 2.1_
* [.Net Core 2.1](https://dotnet.microsoft.com/download/dotnet-core/2.1) - Acá puedes descargarlo


## Ejecución del Servicio
_Únicamente basta con descargar los archivos, ingresar a una consola y situarnos en la ruta donde descargamos los archivos._
Manualmente hay que correr el siguiente comando

_Servicio de ESB, correrá en el puerto 8080 y se levanta con la dll cargada del artefacto_

```
dotnet .\esbAPI.dll
```

## Autor ✒️

* **Haroldo Pablo Arias Molina** - *Trabajo Inicial* - [harias25](https://github.com/harias25)
 
## Licencia 📄

Este proyecto está bajo la Licencia Libre - mira el archivo [LICENSE.md](LICENSE.md) para detalles